#pragma once
#include <vector>

using namespace std;

class Game
{
private:
	vector<int> m_scores;
	int m_times[21];
public:
	Game(void);
	~Game(void);


	void Roll(int pins)
	{
		int currentBall = m_scores.size();

		m_scores.push_back(pins);
		if(currentBall < 18 && pins == 10 && currentBall % 2 == 0) // strike
		{
			m_scores.push_back(0);
		}
		
	}

	int GetScore() 
	{
		for(int i=0;i<21;++i)m_times[i]=1;
		for (int i = 0; i < m_scores.size(); i++)
		{
			int pins = m_scores[i];
			if(i < 18 && pins == 10 && i % 2 == 0) // strike
			{
				m_times[i + 2] += 1;
				if(m_scores[i + 2] == 10) // strike
				{
					m_times[i + 4] += 1;
				}
				else
				{
					m_times[i + 3] += 1;
				}
			}
			else if(i % 2 == 1 && pins!=0 && (pins+m_scores[i-1]) == 10)//spare
			{
				m_times[i + 1] += 1;
			}
		}

		int sum = 0;
		for (int i = 0; i < m_scores.size(); i++)
		{
			//if(i < 18 &&  i % 2 == 0 && m_scores[i] == 10) { // strike
			//	sum += m_scores[i+1] + m_scores[i+2];
			//}
			sum += m_scores[i]*m_times[i];
		}
		return sum;
	}
};

// 1     2    3
// 10 0, 10 0, 0 10
// 0  1  2 3  4 5
// 0  0  1 1  2 2
