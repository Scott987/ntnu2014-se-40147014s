#include "gtest/gtest.h"
#include <string>
#include "Game.h"
using namespace std;

class MyTest : public ::testing::Test
{
};


TEST_F(MyTest, AllZero)
{
	Game g;
	for (int i = 0; i < 20; i++)
	{
		g.Roll(0);
	}
	
	EXPECT_EQ(0 * 20, g.GetScore());
}

TEST_F(MyTest, AllOne)
{
	Game g;
	for (int i = 0; i < 20; i++)
	{
		g.Roll(1);
	}
	EXPECT_EQ(1 * 20, g.GetScore());
}

TEST_F(MyTest, AllStrike)
{
	Game g;
	// frame 1~9
	for (int i = 0; i < 9; i++)
	{
		g.Roll(10);
	}

	// frame 10
	for (int i = 0; i < 3; i++)
	{
		g.Roll(10);
	}

	EXPECT_EQ(300, g.GetScore());
}

TEST_F(MyTest,FinalAllStrike)
{
	Game g;
	// frame 1~9
	for (int i = 0; i < 18; i++)
	{
		g.Roll(1);
	}

	// frame 10
	for (int i = 0; i < 3; i++)
	{
		g.Roll(10);
	}

	EXPECT_EQ(1*18+30, g.GetScore());
}

TEST_F(MyTest,AllSpare)
{
	Game g;
	for (int i = 0; i < 21; i++)
	{
		g.Roll(5);
	}

	EXPECT_EQ(155, g.GetScore());
}

TEST_F(MyTest,AllSpareTenthStrike)
{
	Game g;
	for (int i = 0; i < 18; i++)
	{
		g.Roll(5);
	}
	g.Roll(10);
	g.Roll(5);
	g.Roll(5);

	EXPECT_EQ(5*26+10+10+5+5, g.GetScore());
}

TEST_F(MyTest,AllSpareLastThreeStrike)
{
	Game g;
	for (int i = 0; i < 18; i++)
	{
		g.Roll(5);
	}
	g.Roll(10);
	g.Roll(10);
	g.Roll(10);

	EXPECT_EQ(5*26+10+10+10+10, g.GetScore());
}